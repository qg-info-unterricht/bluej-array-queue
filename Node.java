
/**
 * Klasse Node
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class Node<T>
{
    /**
     * Der Datenwert des Listenknotens
     */
    private T data;

    /**
     * Der Nachfolger des Listenknotens
     */
    private Node<T> next;

    /**
     * Erzeugt einen neuen Listenknoten
     * 
     * @param daten Der Datenwert des Knotens
     * @param nachfolger Der Nachfolger des Knotens
     */
    public Node(T daten, Node<T> nachfolger)
    {
        this.data = daten;
        this.next = nachfolger;
    }

    /**
     * Setzt den Folgeknoten
     * 
     * @param  nextNode    Nächster Knoten
     */
    public void setNext(Node nextNode)
    {
        this.next = nextNode;
    }
    
    /**
     * Holt den Folgeknoten
     * 
     * @return  nextNode  Nächster Knoten
     */
    public Node getNext()
    {
        return this.next;
    }
    
    /**
     * Liest das Inhaltsobjekt
     * 
     * @return  content  Inhalt
     */
    public T getContent()
    {
        return this.data;
    }    

}
